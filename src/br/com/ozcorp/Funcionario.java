package br.com.ozcorp;

public class Funcionario {

	private String nome;
	private String rg;
	private String cpf;
	private String matricula;
	private String email;
	private String senha;
	private TipoSanguineo tipoSanguineo;
	private Sexo sexo;
	private NivelDeAcesso nivelDeAcesso;
	private Departamento departamento;
	
	// Construtor
	public Funcionario(String nome, String rg, String cpf, String matricula, String email, Departamento departamento,
			String senha, Sexo sexo, TipoSanguineo tipoSanguineo, NivelDeAcesso nivelDeAcesso) {

		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		this.email = email;
		this.senha = senha;
		this.tipoSanguineo = tipoSanguineo;
		this.sexo = sexo;
		this.nivelDeAcesso = nivelDeAcesso;
		this.departamento = departamento;
		
	}

	// Getters and setters
	public NivelDeAcesso getNivelDeAcesso() {
		return nivelDeAcesso;
	}

	public void setNivelDeAcesso(NivelDeAcesso nivelDeAcesso) {
		this.nivelDeAcesso = nivelDeAcesso;
	}
	
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public TipoSanguineo getTipoSanguineo() {
		return tipoSanguineo;
	}

	public void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

}
