package br.com.ozcorp;

public enum Sexo {
	
	MASCULINO("Masculino"), FEMININO("Feminino"), OUTRO("Outro");
	
	public String s;
	
	Sexo(String s){
		this.s = s;
	}

}
