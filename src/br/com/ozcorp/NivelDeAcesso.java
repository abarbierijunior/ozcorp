package br.com.ozcorp;

public enum NivelDeAcesso {
	DIRETOR("Zero"), SECRETARIO("Um"), GERENTE("Dois"), ENGENHEIRO("Tr�s"), ANALISTA("Quatro");
	
	public String nivel;
	
	NivelDeAcesso(String nivel){
		this.nivel = nivel;
	}
}
