package br.com.ozcorp;

public class Cargo {

	private String titulo;
	private double salarioBase;
	
	// Construtor
	public Cargo(String titulo, double salarioBase) {
		
		this.titulo = titulo;
		this.salarioBase = salarioBase;
	}

	
	// Getters and setters 
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}
	
	
	
	
		
}
