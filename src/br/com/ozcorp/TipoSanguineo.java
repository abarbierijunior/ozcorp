package br.com.ozcorp;

public enum TipoSanguineo {

	APOSITIVO("A+"), ANEGATIVO("A-"), BPOSITIVO("B+"), BNEGATIVO("B-"),
	ABPOSITIVO("AB+"), ABNEGATIVO("AB-"), OPOSITIVO("O+"), ONEGATIVO("O-");
	
	public String ts;
	
	TipoSanguineo(String ts){
		this.ts = ts;
	}
}
