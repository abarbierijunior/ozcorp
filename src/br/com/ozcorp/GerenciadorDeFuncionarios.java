package br.com.ozcorp;

/**
 * Programa que gerencia os funcion�rios
 * de uma determinada empresa.
 * 
 * @author Matheus Barbieri de Ara�jo
 *
 */

public class GerenciadorDeFuncionarios {

	public static void main(String[] args) {
		Funcionario John = new Funcionario("John", "562365", "48952", "kjb-4186", "john@br", new Departamento("Financeiro",
				"AF", new Cargo("Analista", 12_789)), "123", Sexo.MASCULINO, TipoSanguineo.ABNEGATIVO, NivelDeAcesso.ANALISTA);
		
		System.out.println("DADOS DO FUNCION�RIO \n");	
		System.out.println("Nome: " + John.getNome());
		System.out.println("RG: " + John.getRg());
		System.out.println("CPF: " + John.getCpf());
		System.out.println("Matr�cula: " + John.getMatricula());
		System.out.println("E-mail: " + John.getEmail());
		System.out.println("Senha: " + John.getSenha());
		System.out.println("Cargo: " + John.getDepartamento().getCargo().getTitulo());
		System.out.println("Sal�rio Base: " +John.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sangu�neo: " + John.getTipoSanguineo().ts);
		System.out.println("Sexo: " + John.getSexo());
		System.out.println("Fun��o: " + John.getDepartamento().getFuncao());
		System.out.println("Sigla: " + John.getDepartamento().getSigla());
		System.out.println("N�vel de Acesso: " + John.getNivelDeAcesso().nivel);

	}

}
