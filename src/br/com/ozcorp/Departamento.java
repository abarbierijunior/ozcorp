package br.com.ozcorp;

public class Departamento {

	private String funcao;
	private String sigla;
	private Cargo cargo;
	
	// Construtor
	public Departamento(String funcao, String sigla, Cargo cargo) {
		this.funcao = funcao;
		this.sigla = sigla;
		this.cargo = cargo;
	}

	// Getters and setters
	public String getFuncao() {
		return funcao;
	}
	
	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	
	


}
